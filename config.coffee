path = require 'path'
express = require 'express'
compiler = require 'connect-compiler'

module.exports = (app) ->
  publicDir = path.join __dirname, 'public'
  buildDir = path.join __dirname, 'build'
  publicStylesDir = path.join publicDir, 'styles'

  app.set 'view engine', 'hbs'
  app.use express.bodyParser()
  app.use express.logger(format: ':method :url :status :response-time ms')
  app.use app.router

  app.use compiler(
    enabled: ['coffee', 'less']
    src: publicDir
    dest: buildDir
    options:
      less:
        paths: [publicStylesDir]
  )
  app.use express.static(publicDir)
  app.use express.static(buildDir)