
user = require './controllers/user'
messages = require './controllers/messages'

module.exports = (app, io) ->

  app.get '/user', user.test

  app.get '/messages', messages.get

  app.post '/input', messages.post


  io.sockets.on 'connection', (socket) ->


#    socket.emit 'news', { hello: 'world' });
#      socket.on('my other event', function (data) {
#      console.log(data);
