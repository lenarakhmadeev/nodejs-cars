
http = require 'http'
express = require 'express'
socketio = require 'socket.io'

# Server
app = express()
server = http.createServer app
io = socketio.listen server


mongoose = require 'mongoose'
mongoose.connect 'localhost', 'test'

# Configure app
require('./config') app

# Routing
require('./router') app, io

# Start server
server.listen 3000
