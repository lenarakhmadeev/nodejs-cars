
apiId = '3088438' # номер вашего приложения
apiSecret = '8bUOws7p2K6v6BPgLYOg' # секретный ключ приложения из Настроек

crypto = require 'crypto'


module.exports =
  test: (req, res) ->
    authKey = req.query.auth_key
    viewerId = req.query.viewer_id

    hash = crypto
      .createHash('md5')
      .update("#{apiId}_#{viewerId}_#{apiSecret}")
      .digest('hex')

    result =
      query: req.query
      auth: hash == authKey

    res.json result