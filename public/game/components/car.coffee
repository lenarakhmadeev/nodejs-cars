define (require)->
	Crafty = require('Crafty')


	Crafty.c 'Car',
		init: ()->
			@bind('EnterFrame', @makeStep)
			@acceleration = 0.65
			@brake_step = 0.3
			@max_speed = 100


		makeStep: ()->
			@adjustRotation()
			@adjustAcceleration()
			@adjustImpulse()
			@damping()


		adjustAcceleration: ()->
			@speed = 0

			if @controls.up
				@speed = @acceleration

			if @controls.down
				@speed = -@brake_step


		adjustImpulse: ()->
			if @speed
				impulseModule = @speed
				angle = @body.GetAngle()

				xImpulse = Math.sin(angle) * impulseModule
				yImpulse = -Math.cos(angle) * impulseModule

				impulse = new b2Vec2(xImpulse, yImpulse)
				@body.ApplyImpulse(impulse, @body.GetWorldCenter())


		rotationDelta: ()->
			0.5 * @body.GetLinearVelocity().Length()

		adjustRotation: ()->
			if @controls.right
				@rotate(@rotationDelta())

			if @controls.left
				@rotate(-@rotationDelta())

		damping: ()->
			eps = 0.91

			linearVelocity = @body.GetLinearVelocity()
			bodyAngle = @body.GetAngle()

			P = 0.9

			ny = @getAngleVec(bodyAngle)
			nx = @getAngleVec(bodyAngle + Math.PI / 2)

			decomp = @solve(linearVelocity, nx, ny)

			nx.Multiply(decomp.x)
			ny.Multiply(P * decomp.y)

			nx.Add(ny)

			f = linearVelocity.Subtract(nx)

			@body.SetLinearVelocity(nx)


		getAngleVec: (angle)->
			x = Math.cos(angle)
			y = Math.sin(angle)

			new b2Vec2(x, y)


		getVecAngle: (vec)->
			-Math.atan2(-vec.x, -vec.y)


		_fun: (x)->
			0.5 - Math.cos(2 * x) / 2

		solve: (vec, axisX, axisY)->
			D = @det(axisX.x, axisY.x, axisX.y, axisY.y)
			D1 = @det(vec.x, axisY.x, vec.y, axisY.y)
			D2 = @det(axisX.x, vec.x, axisX.y, vec.y)

			new b2Vec2(D1 / D, D2 / D)

		det: (a11, a12, a21, a22)->
			a11 * a22 - a12 * a21


		rotate: (angle)->
			desiredAngle = Crafty.math.degToRad(angle)
			bodyAngle = @body.GetAngle()

			@body.SetAngle(bodyAngle + desiredAngle)

