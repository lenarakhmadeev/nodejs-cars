define (require)->
	Crafty = require('Crafty')


	Crafty.c 'User',

		init: ()->
			@addComponent('Keyboard')

			@controls = {}
			@controlButtons =
				UP: Crafty.keys.W
				DOWN: Crafty.keys.S
				LEFT: Crafty.keys.A
				RIGHT: Crafty.keys.D

			@bind('EnterFrame', @_onEnterFrame)


		_onEnterFrame: ()->
			@controls.up = @isDown(@controlButtons.UP)
			@controls.down = @isDown(@controlButtons.DOWN)
			@controls.left = @isDown(@controlButtons.LEFT)
			@controls.right = @isDown(@controlButtons.RIGHT)

