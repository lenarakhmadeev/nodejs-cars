define (require)->
	Crafty = require('Crafty')
	C = require('config_')


	Crafty.c 'FollowCar',
		init: ()->
			Crafty.viewport.init(C.VIEWPORT_WIDTH, C.VIEWPORT_HEIGHT)
			Crafty.viewport.centerOn(this)
			#Crafty.viewport.follow(@, 0, 0)

			console.log(@)

			@bind('EnterFrame', @onEnterFrame)


		onEnterFrame: ()->
			vpx = (@.x - C.VIEWPORT_WIDTH / 2)
			vpy = (@.y - C.VIEWPORT_HEIGHT / 2)

			Crafty.viewport.x = -vpx
			Crafty.viewport.y = -vpy