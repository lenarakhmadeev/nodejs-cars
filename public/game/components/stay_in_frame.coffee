define (require)->
	Crafty = require('Crafty')
	C = require('config_')


	Crafty.c 'StayInFrame',
		init: ()->
			this.requires('Box2D')

			@bind('EnterFrame', @onEnterFrame)

		setX: (x)->
			currentPosition = @body.GetPosition()
			newPosition = new b2Vec2(x, currentPosition.y)
			@body.SetPosition(newPosition)

		setY: (y)->
			currentPosition = @body.GetPosition()
			newPosition = new b2Vec2(currentPosition.x, y)
			@body.SetPosition(newPosition)


		onEnterFrame: ()->
			if @x > C.SCENE_WIDTH
				@setX(0)

			if @x < 0
				@setX(C.SCENE_WIDTH / C.METR)

			if @y > C.SCENE_HEIGHT
				@setY(0)

			if @y < 0
				@setY(C.SCENE_HEIGHT / C.METR)
