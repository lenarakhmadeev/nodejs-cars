define (require)->
	Crafty = require('Crafty')
	C = require('config_')
	require('sprites')
	require('components/car')
	require('components/stay_in_frame')


	(x, y, sprite)->
		components = ['2D', 'Canvas', 'Box2D', 'Car', 'StayInFrame', sprite].join(', ')

		car = Crafty.e(components)
		car.attr
			x: x
			y: y
			z: 1
			w: C.CAR_WIDTH
			h: C.CAR_HEIGTH

		car.box2d
			bodyType: 'dynamic'
			density: 8 # Плотность
			friction: 1 # Трение
			restitution: 0.2 # Упрогость
			dampingRatio: 0.1

		car.body.SetLinearDamping(0.2)
		car.body.SetAngularDamping(0.3)

		car