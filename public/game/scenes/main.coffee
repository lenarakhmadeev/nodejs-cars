define (require)->
	Crafty = require('Crafty')
	Car = require('entities/car')
	C = require('config_')
	require('components/follow_car')
	require('components/user')
	require('sprites')

	Crafty.scene 'main', ()->
		player1 = Car(100, 100, 'playerCarSprite')
		player1.addComponent('User')

		player2 = Car(200, 200, 'npcCarSprite')
		player2.addComponent('User')
		player2.attr
			controlButtons:
				UP: Crafty.keys.UP_ARROW
				DOWN: Crafty.keys.DOWN_ARROW
				LEFT: Crafty.keys.LEFT_ARROW
				RIGHT: Crafty.keys.RIGHT_ARROW



