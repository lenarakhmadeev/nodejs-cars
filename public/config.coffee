
'use strict'

require.config
  # Начальная точка приложения
  deps: ['main']
  baseUrl: 'scripts'
  paths:
    # Библиотеки
    Crafty: 'libs/crafty/crafty'
    Box2D: 'libs/crafty/Box2dWeb-2.1.a.3'
    Socketio: 'libs/socket.io/socket.io'
    Backbone: 'libs/backbone/backbone'
    _: 'libs/lodash'
    $: 'libs/jquery-1.8.2'

    # Плагины requirejs
    text: 'libs/requirejs/text'
    tpl: 'libs/requirejs/tpl'

    # Плагины Backbone
    backbone_ioSync: 'libs/backbone/backbone.iosync'
    backbone_ioBind: 'libs/backbone/backbone.iobind'

    # Компоненты Crafty
    crafty_box2d: 'libs/crafty/craftybox2d'

  map:
    '*':
      jquery: '$'
      underscore: '_'
      backbone: 'Backbone'

  # Загрузка не AMD модулей
  shim:
    Crafty:
      exports: 'Crafty'

    Backbone:
      deps: ['_', '$']
      exports: ()->
        # Хак, чтобы убрать _ из window
        this._.noConflict()
        this.Backbone.noConflict()

    $:
      exports: ()->
        # Хак для плагинов
        require ['libs/bootstrap'], ()->
          window.jQuery.noConflict()

        window.jQuery

    crafty_box2d:
      deps: ['Crafty', 'Box2D']


  # Константы для модулей
  config:
  # url для socket.io
    'main':
      ioUrl: 'http://localhost'



