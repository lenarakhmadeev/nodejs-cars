define (require)->
	Backbone = require('Backbone')
	Crafty = require('Crafty')
	C = require('config_')
	require('Box2D')
	require('crafty_box2d')
	require('scenes/main')


	class PlaygroundView extends Backbone.View

		initialize: (options)->
			Crafty.init(C.SCENE_WIDTH, C.SCENE_HEIGHT)
			Crafty.canvas.init()
			Crafty.box2D.init(0, 0, C.METR, true)
			Crafty.background('#555555')


		render: ()->
			Crafty.scene('main')

