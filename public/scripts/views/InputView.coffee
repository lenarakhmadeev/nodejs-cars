define (require) ->
  Backbone = require 'Backbone'
  $ = require '$'
  template = require 'tpl!input'


  class InputView extends Backbone.View

    className: 'input'

    events:
      'keyup input': 'changeText'

    initialize: ->
      console.log 'INP'


    render: ->
      tplHtml = template()
      @$el.html tplHtml


    changeText: (event) ->
      text = $(event.target).val()
      @model.save text: text