define (require) ->
  Backbone = require 'Backbone'

  MessagesCollection = require 'models/MessagesCollection'
  MessagesView = require 'views/MessagesView'

  InputModel = require 'models/InputModel'
  InputView = require 'views/InputView'


  class ChatView extends Backbone.View

    initialize: ->
      messagesCollection = new MessagesCollection()
      messagesCollection.fetch()
      @messagesView = new MessagesView collection: messagesCollection

      inputModel = new InputModel()
      @inputView = new InputView model: inputModel


    render: ->
      @messagesView.render()
      @$el.append @messagesView.el

      @inputView.render()
      @$el.append @inputView.el
