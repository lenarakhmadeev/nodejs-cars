define (require)->

	View = require('views/View')
	PlaygroundView = require('views/PlaygroundView')
	StatsView = require('views/StatsView')
	UsersCollection = require('models/UsersCollection')
	appTemplate = require('tpl!app')


	class AppView extends View

		template: appTemplate

		initialize: (options)->
			usersCollection = new UsersCollection()
			@statsView = new StatsView(collection: usersCollection)


		_render: ()->
			@playgroundView = new PlaygroundView(el: @$('.playground-place'))
			@playgroundView.render()

			@append('.stats-place', @statsView)
			@statsView.render()

