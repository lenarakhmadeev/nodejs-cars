define (require) ->
  Backbone = require 'Backbone'
  MessageView = require 'views/MessageView'


  class MessagesView extends Backbone.View

    initialize: ->
      @listenTo @collection, 'reset', @render


    render: ->
      @$el.empty()
      @collection.each @addOne, this


    addOne: (model) ->
      messageView = new MessageView {model}
      messageView.render()

      @$el.append messageView.el



