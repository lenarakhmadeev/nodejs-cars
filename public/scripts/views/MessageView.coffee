define (require) ->
  Backbone = require 'Backbone'
  template = require 'tpl!message'


  class MessagesView extends Backbone.View

    render: ->
      data = @model.toJSON()
      tplHtml = template data
      @$el.html tplHtml


