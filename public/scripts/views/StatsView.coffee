define (require)->

	View = require('views/View')
	_ = require('_')
	statsTemplate = require('tpl!stats')


	class StatsView extends View

		template: statsTemplate

		className: 'b-stats'

		initialize: (options)->
			@collection.on('all', @render, this)


		serialize: ()->
			users = @collection.toJSON()

			users: _.sortBy(users, (user)-> -user.score)