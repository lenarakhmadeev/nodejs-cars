// Defines a module that works in Node and AMD.

// This version can be used as common boilerplate for a library module
// that you only want to expose to Node and AMD loaders. It will not work
// well for defining browser globals.

// If you need a version of this file that works CommonJS-like environments
// that do not support module.exports or if you want to define a module
// with a circular dependency, see commonjsAdapter.js

// Help Node out by setting up define.
if (typeof module === 'object' && typeof define !== 'function') {
	var define = function (factory) {
		module.exports = factory(require, exports, module);
	};
}

define(function (require, exports, module) {
	var Backbone = require('backbone');
	var _ = require('underscore');
	var $ = require('jquery');


	return function () {
		/*!
		 * backbone.iobind - Backbone.sync replacement
		 * Copyright(c) 2011 Jake Luer <jake@alogicalparadox.com>
		 * MIT Licensed
		 */


		/**
		 * # Backbone.sync
		 *
		 * Replaces default Backbone.sync function with socket.io transport
		 *
		 * ### Assumptions
		 *
		 * Currently expects active socket to be located at `window.socket`,
		 * `Backbone.socket` or the sync'ed model own socket.
		 * See inline comments if you want to change it.
		 * ### Server Side
		 *
		 *     socket.on('todos:create', function (data, fn) {
		 *      ...
		 *      fn(null, todo);
		 *     });
		 *     socket.on('todos:read', ... );
		 *     socket.on('todos:update', ... );
		 *     socket.on('todos:delete', ... );
		 *
		 * @name sync
		 */
		Backbone.sync = function (method, model, options) {
			var getUrl = function (object) {

				if (options && options.url) {
					return _.isFunction(options.url) ? options.url() : options.url;
				}

				if (!(object && object.url)) return null;
				return _.isFunction(object.url) ? object.url() : object.url;
			};

			var cmd = getUrl(model).split('/')
				, namespace = (cmd[0] !== '') ? cmd[0] : cmd[1]; // if leading slash, ignore

			var params = _.extend({
				req: namespace + ':' + method
			}, options);

			if ( !params.data && model ) {
				params.data = model.toJSON() || {};
			}

			// If your socket.io connection exists on a different var, change here:
			var io = model.socket || window.socket || Backbone.socket;

			var defer = $.Deferred();
			io.emit(namespace + ':' + method, params.data, function (err, data) {
				if (err) {
					options.error(err);
					defer.reject();
				} else {
					options.success(data);
					defer.resolve();
				}
			});
			return defer.promise();
		};
	};
});