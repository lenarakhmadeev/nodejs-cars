define (require) ->
  $ = require '$'
  ChatView = require 'views/ChatView'


  chatView = new ChatView()
  chatView.render()

  $('body').html chatView.el
