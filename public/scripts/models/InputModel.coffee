define (require) ->
  Backbone = require 'Backbone'


  class InputModel extends Backbone.Model

    url: '/input'