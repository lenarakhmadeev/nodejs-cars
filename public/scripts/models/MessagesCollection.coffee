define (require) ->
  Backbone = require 'Backbone'


  class MessagesCollection extends Backbone.Collection

    url: '/messages'
