define (require)->
	Backbone = require('Backbone')
	_ = require('_')
	UserModel = require('models/UserModel')

	class UsersCollection extends Backbone.Collection

		model: UserModel

		url: 'users'

		initialize: ()->
			@fetch()
			@create lol: 12

