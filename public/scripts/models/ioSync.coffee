define (require, exports, module)->
	Socketio = require('Socketio')
	_ = require('_')


	ioUrl = module.config().ioUrl
	io = Socketio.connect(ioUrl)

	sync = (method, model, options)->
		console.log 'sync', arguments

		getUrl = (object)->
			if options and options.url
				object = options

			if _.isFunction(object.url) then object.url() else object.url;


		cmd = getUrl(model).split('/')
		namespace = if not cmd[0] == '' then cmd[0] else cmd[1] # if leading slash, ignore

		url = namespace + ':' + method
		data = options.data or model.toJSON()

		io.emit url, data, (err, data)->
			console.log 'sync callback', arguments

			if err
				options.error(err)
			else
				options.success(data)

